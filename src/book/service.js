import {getLogger} from '../core/utils';
import {apiUrl, authHeaders} from '../core/api';
const log = getLogger('book/service');
const action = (type, payload) => ({type, payload});

const SAVE_BOOK_STARTED = 'book/saveStarted';
const SAVE_BOOK_SUCCEEDED = 'book/saveSucceeded';
const SAVE_BOOK_FAILED = 'book/saveFailed';
const CANCEL_SAVE_BOOK = 'book/cancelSave';

const LOAD_BOOKS_STARTED = 'book/loadStarted';
const LOAD_BOOKS_SUCCEEDED = 'book/loadSucceeded';
const LOAD_BOOKS_FAILED = 'book/loadFailed';
const CANCEL_LOAD_BOOKS = 'book/cancelLoad';

const DELETE_BOOK_STARTED = 'book/deleteStarted';
const DELETE_BOOK_SUCCEEDED = 'book/deleteSucceeded';
const DELETE_BOOK_FAILED = 'book/deleteFailed';
const CANCEL_DELETE_BOOK = 'book/cancelDelete';

export const loadBooks = () => (dispatch, getState) => {
  log(`loadBooks started`);
  dispatch(action(LOAD_BOOKS_STARTED));
  let ok = false;
  return fetch(`${apiUrl}/book`, {method: 'GET', headers: authHeaders(getState().auth.token)})
    .then(res => {
      ok = res.ok;
      return res.json();
    })
    .then(json => {
      log(`loadBooks ok: ${ok}, json: ${JSON.stringify(json)}`);
      if (!getState().book.isLoadingCancelled) {
        dispatch(action(ok ? LOAD_BOOKS_SUCCEEDED : LOAD_BOOKS_FAILED, json));
      }
    })
    .catch(err => {
      log(`loadBooks err = ${err.message}`);
      if (!getState().book.isLoadingCancelled) {
        dispatch(action(LOAD_BOOKS_FAILED, {issue: [{error: err.message}]}));
      }
    });
};
export const cancelLoadBooks = () => action(CANCEL_LOAD_BOOKS);

export const saveBook = (book) => (dispatch, getState) => {
  const body = JSON.stringify(book);
  log(`saveBook started`);
  dispatch(action(SAVE_BOOK_STARTED));
  let ok = false;
  const url = book._id ? `${apiUrl}/book/${book._id}` : `${apiUrl}/book`;
  const method = book._id ? `PUT` : `POST`;
  return fetch(url, {method, headers: authHeaders(getState().auth.token), body})
    .then(res => {
      ok = res.ok;
      return res.json();
    })
    .then(json => {
      log(`saveBook ok: ${ok}, json: ${JSON.stringify(json)}`);
      if (!getState().book.isSavingCancelled) {
        dispatch(action(ok ? SAVE_BOOK_SUCCEEDED : SAVE_BOOK_FAILED, json));
      }
    })
    .catch(err => {
      log(`saveBook err = ${err.message}`);
      if (!getState().isSavingCancelled) {
        dispatch(action(SAVE_BOOK_FAILED, {issue: [{error: err.message}]}));
      }
    });
};

export const deleteBook = (book) => (dispatch, getState) => {
  const body = JSON.stringify(book);
  log(`deleteBook started`);
  dispatch(action(DELETE_BOOK_STARTED));
  let ok = false;
  if(book._id){
    const url = `${apiUrl}/book/${book._id}`;
    const method = `DELETE`;
    return fetch(url, {method, headers: authHeaders(getState().auth.token), body})
        .then(res => {
          ok = res.ok;
          return res.json();
        })
        .then(json => {
          log(`deleteBook ok: ${ok}, json: ${JSON.stringify(json)}`);
          if (!getState().book.isSavingCancelled) {
            dispatch(action(ok ? DELETE_BOOK_SUCCEEDED : DELETE_BOOK_FAILED, json));
          }
        })
        .catch(err => {
          log(`deleteBook err = ${err.message}`);
          if (!getState().isSavingCancelled) {
            dispatch(action(DELETE_BOOK_FAILED, {issue: [{error: err.message}]}));
          }
        });
  }

};

export const cancelSaveBook = () => action(CANCEL_SAVE_BOOK);

export const bookReducer = (state = {items: [], isLoading: false, isSaving: false}, action) => { //newState (new object)
  switch(action.type) {
    case LOAD_BOOKS_STARTED:
      return {...state, isLoading: true, isLoadingCancelled: false, issue: null};
    case LOAD_BOOKS_SUCCEEDED:
      return {...state, items: action.payload, isLoading: false};
    case LOAD_BOOKS_FAILED:
      return {...state, issue: action.payload.issue, isLoading: false};
    case CANCEL_LOAD_BOOKS:
      return {...state, isLoading: false, isLoadingCancelled: true};
    case SAVE_BOOK_STARTED:
      return {...state, isSaving: true, isSavingCancelled: false, issue: null};
    case SAVE_BOOK_SUCCEEDED:
      let items = [...state.items];
      let index = items.findIndex((i) => i._id == action.payload._id);
      if (index != -1) {
        items.splice(index, 1, action.payload);
      } else {
        items.push(action.payload);
      }
      return {...state, items, isSaving: false};
    case SAVE_BOOK_FAILED:
      return {...state, issue: action.payload.issue, isSaving: false};
    case CANCEL_SAVE_BOOK:
      return {...state, isSaving: false, isSavingCancelled: true};

    case DELETE_BOOK_STARTED:
      return {...state, isSaving: true, isSavingCancelled: false, issue: null};
    case DELETE_BOOK_SUCCEEDED:
      if (index != -1) {
        items.splice(index, 1, action.payload);
      } else {
        items.push(action.payload);
      }
      return {...state, items, isSaving: false};
    case DELETE_BOOK_FAILED:
      return {...state, issue: action.payload.issue, isSaving: false};
    case CANCEL_DELETE_BOOK:
      return {...state, isSaving: false, isSavingCancelled: true};
    default:
      return state;
  }
};

