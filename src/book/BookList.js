import React, {Component} from 'react';
import {ListView, Text, View, StatusBar, ActivityIndicator} from 'react-native';
import {BookEdit} from './BookEdit';
import {BookView} from './BookView';
import {loadBooks, cancelLoadBooks} from './service';
import {registerRightAction, getLogger, issueText} from '../core/utils';
import styles from '../core/styles';

const log = getLogger('BookList');
const BOOK_LIST_ROUTE = 'book/list';

export class BookList extends Component {
  static get routeName() {
    return BOOK_LIST_ROUTE;
  }

  static get route() {
    return {name: BOOK_LIST_ROUTE, title: 'Book List', rightText: 'New'};
  }

  constructor(props) {
    super(props);
    log('constructor');
    this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1.id !== r2.id});
    const bookState = this.props.store.getState().book;
    this.state = {isLoading: bookState.isLoading, dataSource: this.ds.cloneWithRows(bookState.items)};
    registerRightAction(this.props.navigator, this.onNewBook.bind(this));
  }

  render() {
    log('render');
    let message = issueText(this.state.issue);
    return (
      <View style={styles.content}>
        { this.state.isLoading &&
        <ActivityIndicator animating={true} style={styles.activityIndicator} size="large"/>
        }
        {message && <Text>{message}</Text>}
        <ListView
          dataSource={this.state.dataSource}
          enableEmptySections={true}
          renderRow={book => (<BookView book={book} onPress={(book) => this.onBookPress(book)}/>)}/>
      </View>
    );
  }

  onNewBook() {
    log('onNewBook');
    this.props.navigator.push({...BookEdit.route});
  }

  onBookPress(book) {
    log('onBookPress');
    this.props.navigator.push({...BookEdit.route, data: book});
  }

  componentDidMount() {
    log('componentDidMount');
    this._isMounted = true;
    const store = this.props.store;
    this.unsubscribe = store.subscribe(() => {
      log('setState');
      const state = this.state;
      const bookState = store.getState().book;
      this.setState({dataSource: this.ds.cloneWithRows(bookState.items), isLoading: bookState.isLoading});
    });
    store.dispatch(loadBooks());
  }

  componentWillUnmount() {
    log('componentWillUnmount');
    this._isMounted = false;
    this.unsubscribe();
    this.props.store.dispatch(cancelLoadBooks());
  }
}
