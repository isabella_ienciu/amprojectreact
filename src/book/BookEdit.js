import React, {Component} from 'react';
import {Text, View, TextInput, ActivityIndicator} from 'react-native';
import {saveBook, cancelSaveBook} from './service';
import {registerRightAction, issueText, getLogger} from '../core/utils';
import styles from '../core/styles';

const log = getLogger('BookEdit');
const BOOK_EDIT_ROUTE = 'book/edit';

export class BookEdit extends Component {
  static get routeName() {
    return BOOK_EDIT_ROUTE;
  }

  static get route() {
    return {name: BOOK_EDIT_ROUTE, title: 'Book Edit', rightText: 'Save', leftText: 'Delete'};
  }

  constructor(props) {
    log('constructor');
    super(props);
    const nav = this.props.navigator;
    const currentRoutes = nav.getCurrentRoutes();
    const currentRoute = currentRoutes[currentRoutes.length - 1];
    if (currentRoute.data) {
      this.state = {book: {...currentRoute.data}, isSaving: false};
    } else {
      this.state = {book: {title: ''}, isSaving: false};
    }
    registerRightAction(this.props.navigator, this.onSave.bind(this));
    registerLeftAction(this.props.navigator, this.onDelete.bind(this));
  }

  render() {
    log('render');
    const state = this.state;
    let message = issueText(state.issue);
    return (
      <View style={styles.content}>
        { state.isSaving &&
        <ActivityIndicator animating={true} style={styles.activityIndicator} size="large"/>
        }
        <Text>Title</Text>
        <TextInput value={state.book.title} onChangeText={(text) => this.updateBookTitle(text)}></TextInput>
        <Text>Author</Text>
        <TextInput value={state.book.author} onChangeText={(text) => this.updateBookAuthor(text)}></TextInput>
        {message && <Text>{message}</Text>}
      </View>
    );
  }

  componentDidMount() {
    log('componentDidMount');
    this._isMounted = true;
    const store = this.props.store;
    this.unsubscribe = store.subscribe(() => {
      log('setState');
      const state = this.state;
      const bookState = store.getState().book;
      this.setState({...state, issue: bookState.issue});
    });
  }

  componentWillUnmount() {
    log('componentWillUnmount');
    this._isMounted = false;
    this.unsubscribe();
    this.props.store.dispatch(cancelSaveBook());
  }

  updateBookTitle(text) {
    let newState = {...this.state};
    newState.book.title = text;
    this.setState(newState);
  }

  updateBookAuthor(text) {
    let newState = {...this.state};
    newState.book.author = text;
    this.setState(newState);
  }

  onSave() {
    log('onSave');
    this.props.store.dispatch(saveBook(this.state.book)).then(() => {
      log('onBookSaved');
      if (!this.state.issue) {
        this.props.navigator.pop();
      }
    });
  }

  onDelete() {
    log('onDelete');
    this.props.store.dispatch(deleteBook(this.state.book)).then(() => {
      log('onBookSaved');
      if (!this.state.issue) {
        this.props.navigator.pop();
      }
    });
  }
}