/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import React, {Component} from 'react';
import {AppRegistry} from 'react-native';
import {createStore, applyMiddleware, combineReducers} from 'redux';
import createLogger from 'redux-logger';
import thunk from 'redux-thunk';
import {bookReducer} from './src/book';
import {authReducer} from './src/auth';
import {Router} from './src/Router'

const rootReducer = combineReducers({book: bookReducer, auth: authReducer});
const store = createStore(rootReducer, applyMiddleware(thunk, createLogger()));
// const store = createStore(rootReducer, applyMiddleware(thunk));

export default class K10 extends Component {
  render() {
    return (
      <Router store={store}/>
    );
  }
}

AppRegistry.registerComponent('K10', () => K10);
